package com.serenitybdd.exercise.features.search;

import com.serenitybdd.exercise.ui.*;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.questions.page.TheWebPage;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import com.serenitybdd.exercise.tasks.*;
import com.serenitybdd.exercise.tasks.Search;

import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static net.serenitybdd.screenplay.EventualConsequence.eventually;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;


@RunWith(SerenityRunner.class)
public class SearchPageStory {

    Actor anna = Actor.named("Anna");

    @Managed(uniqueSession = true)
    public WebDriver herBrowser;
    public GoogleSearchPage googleSearchPage;
    public FindTheElement googleElement;


    @Steps
    OpenTheApplication openTheApplication;

    @Before
    public void annaCanBrowseTheWeb() {

        anna.can(BrowseTheWeb.with(herBrowser));

    }

    @After
    public void annaCanCloseTheBrowser() {
        herBrowser.quit();
    }


    @Test
    public void user_can_view_site() {
        givenThat(anna).wasAbleTo(openTheApplication);

        when(anna).attemptsTo(Open.browserOn().the(googleSearchPage));

        assertThat(openTheApplication.checkCurrentURL(herBrowser), containsString("google.com"));
    }

    @Test
    public void user_can_see_the_links_in_homepage() {
        givenThat(anna).wasAbleTo(openTheApplication);

        when(anna).attemptsTo(Open.browserOn().the(googleSearchPage));

        assertThat( googleElement.findElement(herBrowser, GoogleHomeElements.BOTTOM_LEFT_LINKS), is(true));
        assertThat( googleElement.findElement(herBrowser, GoogleHomeElements.BOTTOM_RIGHT_LINKS), is(true));
        assertThat( googleElement.findElement(herBrowser, GoogleHomeElements.TOP_LEFT_LINKS), is(true));
    }

    @Test
    public void search_results_should_show_the_search_term_in_the_title() {
        givenThat(anna).wasAbleTo(openTheApplication);

        when(anna).attemptsTo(Search.forTheTerm("BDD In Action"));

        then(anna).should(eventually(seeThat(TheWebPage.title(), containsString("BDD In Action"))));
    }


}