package com.serenitybdd.exercise.tasks;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class FindTheElement{


    public static boolean findElement(WebDriver driver, String el) {
      boolean ret_val = true;
      WebElement element = driver.findElement(By.cssSelector(el));

      if (element == null) {
        ret_val = false;
      }
      return ret_val;
    }
}
