package com.serenitybdd.exercise.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Step;
import com.serenitybdd.exercise.ui.GoogleSearchPage;
import org.openqa.selenium.WebDriver;

public class OpenTheApplication implements Task {

    GoogleSearchPage googleSearchPage;

    @Step("Open the application")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
          Open.browserOn().the(googleSearchPage)
        );
    }

    public String checkCurrentURL(WebDriver browser) {
        String currentUrl = browser.getCurrentUrl();
        return currentUrl;
    }

}
