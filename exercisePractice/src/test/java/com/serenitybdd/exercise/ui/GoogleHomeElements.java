package com.serenitybdd.exercise.ui;

public class GoogleHomeElements {

  public static String TOP_LEFT_LINKS = "div#hptl";
  public static String BOTTOM_LEFT_LINKS = "span#fsl";
  public static String BOTTOM_RIGHT_LINKS = "span#fsr";

}
